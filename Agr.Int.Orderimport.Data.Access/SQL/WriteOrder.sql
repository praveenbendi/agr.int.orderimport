﻿BEGIN TRY
DECLARE @Error_Number  INT,
        @ERROR_SEVERITY INT ,
		@Error_Module NVARCHAR(150),
		@Error_Message NVARCHAR(max),
		@Error_Line INT,
		@Error_State INT,
		@End_Message NVARCHAR(max)
-- Declaring constant values as per the analysis in current system
DECLARE @Channel BIGINT = 5637144576,
        @DataReadID NVARCHAR(10)= 'ecm',
		@InventLocationID nvarchar(20)= '7250',
		@TAXGROUP nvarchar(20) = 'AVATAX',
		@TAXITEMGROUP nvarchar(20) = 'AVATAX-ITM',
		@IsNegativeValue INT = -1,
		@instancerelationtype BIGINT = 2975,
		@Languageid VARCHAR(10) = 'en-us',
		--@CustomerNo varchar(20) = '000453481',--[TODO] have to remove if we pass it from XML
		@CustGroup NVARCHAR(10) = 'DEFAULT',
		@PaymMode NVARCHAR(10) = 'CRED', 
		@PaymTermid NVARCHAR(10) = 'CCNet0',
		@NumberFormat NVARCHAR(10) = '000000000', -- Note : This format defined to support AX for current numbers. Total lenth of number is a 10 digit number which will be used via formula
		@AFMItemType INT = 1, -- Default value for service items
		@partyNumber nvarchar(20),
		@PartyRecID BIGINT,
		@TotalNetAmount NUMERIC,
		@NoofItemLines NUMERIC,
		@NoofPaymentLines INT,
		@GrossAmount NUMERIC,
		@NoofItems NUMERIC,
		@SalesPaymentDifference NUMERIC,
		@DLVMode NVARCHAR(40) = 'HD', -- For now this is constant as i didn't side any other delivery mode in transaction table
		@Tendertype int = 1,
		@TransQty numeric = 1.0000000000000000,
		@DefaultAffiliation BIGINT = 5637144576,
		@Exchange numeric = 100.00,
		@Afrcustomershownatp INT = 0,
		@Afmisavailable INT = 1,
		@Afrisscheduledonline INT = 0,
		@Type INT = 27,
		@AfmisAcceptTermsandConditions INT= 1,
		@AfmIsMultiAuthAllowed INT = 1


-- Declaring auto calculated fields
DECLARE @TranID uniqueidentifier,
        @LocationID BIGINT,
		@postalRecid BIGINT 
IF @@TRANCOUNT = 0  
BEGIN
BEGIN TRANSACTION
SET @TranID = newID()

-- Inserting customer data
IF EXISTS (SELECT TOP 1 * FROM [ax].[RETAILTRANSACTIONTABLE] WHERE CHANNELREFERENCEID = @orderno)
BEGIN
 SET @RES = 'false'
 SELECT @RES
 RETURN
END
ELSE
BEGIN
IF NOT EXISTS (SELECT TOP 1 * FROM [ax].[CUSTTABLE] WHERE ACCOUNTNUM = @CustomerNo)
BEGIN
SET ansi_warnings OFF
-- Generate Party number for customer
SELECT @partyNumber = FORMAT(CAST(MAX(PARTYNUMBER) AS BIGINT)+1,@NumberFormat),@PartyRecID =MAX(RECID)+1  FROM [ax].[DIRPARTYTABLE]
INSERT INTO [ax].[DIRPARTYTABLE](RECID,INSTANCERELATIONTYPE,LANGUAGEID,NAME,NAMEALIAS,PARTYNUMBER,CREATEDDATETIME,MODIFIEDDATETIME)
SELECT @PartyRecID,@instancerelationtype,@Languageid,UPPER(@CustomerName),@CustomerName,@partyNumber,@orderdate,getdate()

-- Insert customer data
INSERT INTO [ax].[CUSTTABLE](RECID,ACCOUNTNUM,CURRENCY,CUSTGROUP,PARTY,PAYMMODE,PAYMTERMID,TAXGROUP,CREATEDDATETIME,MODIFIEDDATETIME,DATAAREAID)
SELECT (SELECT MAX(RECID)+1 FROM [ax].[CUSTTABLE]),@CustomerNo,@Currency,@CustGroup,@PartyRecID,@PaymMode,@PaymTermid,@TAXGROUP,@orderdate,GETDATE(),@DataReadID

END
SELECT @postalRecid = max(RECID)+1 FROM [ax].[LOGISTICSPOSTALADDRESS] ----We are not using identity here to support current system for now. This logic will be removed once we generate the Recid from AX
----Insert Shipping postal address
INSERT INTO [ax].[LOGISTICSPOSTALADDRESS] (ADDRESS,COUNTRYREGIONID,ZIPCODE,STATE,CITY,STREET,MODIFIEDDATETIME,RECID,LOCATION) 
SELECT address1+' '+city+','+statecode+' '+postalcode+' '+countrycode,countrycode,postalcode,statecode,city,address1,GETDATE(),@postalRecid,(SELECT max(LOCATION)+1 FROM [ax].[LOGISTICSPOSTALADDRESS]) FROM @ShippingAddress 

-- Insert Billing postal address
INSERT INTO [ax].LOGISTICSPOSTALADDRESS (ADDRESS,COUNTRYREGIONID,ZIPCODE,STATE,CITY,STREET,MODIFIEDDATETIME,RECID,LOCATION) 
SELECT address1+' '+city+','+statecode+' '+postalcode+' '+countrycode,countrycode,postalcode,statecode,city,address1,GETDATE(),@postalRecid+1,(SELECT max(LOCATION)+1 FROM [ax].[LOGISTICSPOSTALADDRESS]) FROM @BillingAddress

-- Auto calculated fields
SELECT @TotalNetAmount = SUM(plt.netprice) + SUM(slt.netprice) FROM  @ProductLineItems plt, @ServiceLineItem slt
--SELECT @NoofItemLines =  COUNT(plt.netprice) + COUNT(slt.netprice) FROM  @ProductLineItems plt, @ServiceLineItem slt
SELECT @NoofItemLines =  count(distinct plt.lineNum)+count(distinct slt.lineNum) FROM  @ProductLineItems plt, @ServiceLineItem slt
SELECT @NoofPaymentLines = COUNT(distinct pmt.lineNum) FROM @Payments pmt
SELECT @GrossAmount = @TotalNetAmount+SUM(plt.tax) + SUM(slt.tax) FROM  @ProductLineItems plt, @ServiceLineItem slt
--SELECT @NoofItems =  sum(distinct plt.Quantity) + sum(distinct slt.Quantity) FROM  @ProductLineItems plt, @ServiceLineItem slt
SELECT @NoofItems =  sum(plt.Quantity) FROM  @ProductLineItems plt
SELECT @NoofItems = @NoofItems +  sum(slt.Quantity) FROM  @ServiceLineItem slt

SELECT @SalesPaymentDifference = SUM(AMOUNT) FROM @Payments
 

-- insert transaction data
--INSERT INTO [ax].[RETAILTRANSACTIONTABLE](CHANNELREFERENCEID,CUSTACCOUNT,RECEIPTEMAIL,CURRENCY,DATAAREAID,TRANSACTIONID,CREATEDDATETIME, MODIFIEDDATETIME) values(@orderno,@CustomerNo,@customeremail,@currency,@DataReadID,@TransactionID,@orderdate,getdate());
INSERT INTO [ax].[RETAILTRANSACTIONTABLE](CHANNELREFERENCEID,TYPE,BUSINESSDATE,TRANSDATE,INVENTLOCATIONID,EXCHRATE,CHANNEL,CUSTACCOUNT,RECEIPTEMAIL,CURRENCY,DATAAREAID,NETAMOUNT,NUMBEROFITEMLINES,NUMBEROFPAYMENTLINES,GROSSAMOUNT,NUMBEROFITEMS,SALESPAYMENTDIFFERENCE,LOGISTICSPOSTALADDRESS,DLVMode,TRANSACTIONID,AFRCUSTOMERSHOWNATP,AFMISACCEPTTERMSANDCONDITIONS,AFMISMULTIAUTHALLOWED,AFMCARDTYPE,ShippingDateRequested,CREATEDDATETIME, MODIFIEDDATETIME,ReceiptDateRequested) 
values(@orderno,@Type,getdate(),getdate(),@InventLocationID,@Exchange,@Channel,@AXCustomerAccountNum,@customeremail,@currency,@DataReadID,@IsNegativeValue*@TotalNetAmount,@NoofItemLines,@NoofPaymentLines,@IsNegativeValue*@GrossAmount,@NoofItems,@SalesPaymentDifference,@logisticPostalAddressID,@DLVMode,@TranID,@Afrcustomershownatp,@AfmisAcceptTermsandConditions,@AfmIsMultiAuthAllowed,(SELECT TOP 1 cardtype FROM @Payments),getdate()+10,getdate(),getdate(),getdate()+10);

--Insert payment transactions
INSERT INTO  [ax].[RETAILTRANSACTIONPAYMENTTRANS] (TRANSACTIONID,LINENUM,CARDTYPEID,AMOUNTTENDERED,CURRENCY,CARDORACCOUNT,QTY,TENDERTYPE,DATAAREAID,CHANNEL,PAYMENTAUTHORIZATION,AFMLINECONFIRMATIONID) 
SELECT @TranID,lineNum,cardtype,amount,currency,cardnumber,@TransQty,@Tendertype,@DataReadID,@Channel,PaymentAuthorization,lineConfirmationId  FROM @Payments -- This schema will be changed and can't insert with existing schema for now

-- Insert product line items
INSERT INTO [ax].[RETAILTRANSACTIONSALESTRANS](CHANNEL,CUSTACCOUNT,INVENTLOCATIONID,ITEMID,AFMItemtype,LINENUM,PRICE,TAXAMOUNT,NETAMOUNT,NETAMOUNTINCLTAX,QTY,TAXGROUP,TAXITEMGROUP,TRANSACTIONID,UNIT,AFMLINECONFIRMATIONID,AFMBEGINDATERANGE,
AFMBESTDATE,
AFMENDDATERANGE,
AFMMESSAGE,AFMVENDACCOUNT,DATAAREAID,AFMISAVAILABLE,AFRISSCHEDULEDONLINE,DLVMODE,LOGISTICSPOSTALADDRESS,ShippingDateRequested,ReceiptDateRequested) 
--values (5637144576,0000088876,@InventLocationID,'T580-3',1,299.9900000000000000,-1.0000000000000000,-17.9900000000000000,'AVATAX','AVATAX-ITM',newID(),'EA','725123231486601','ecm') Kept for sample data
SELECT @Channel,@AXCustomerAccountNum,@InventLocationID,productid,@AFMItemType,Linenum,netprice,@IsNegativeValue*tax,@IsNegativeValue*grossprice,@IsNegativeValue*grossprice,@IsNegativeValue*Quantity,@TAXGROUP,@TAXITEMGROUP,@TranID,Unit,AFMLineConfirmationID,
BeginDateRange,
BestDate,
EndDateRange,
AFMMessage,AFMVendorID,@DataReadID,@Afmisavailable,@Afrisscheduledonline,DLVMode,@logisticPostalAddressID, getdate()+10,getdate()+10 from @ProductLineItems

-- Insert service items
INSERT INTO [ax].[RETAILTRANSACTIONSALESTRANS](CHANNEL,CUSTACCOUNT,INVENTLOCATIONID,ITEMID,LINENUM,NETAMOUNT,NETPRICE,PRICE,TAXAMOUNT,NETAMOUNTINCLTAX,QTY,TAXGROUP,TAXITEMGROUP,TRANSACTIONID,UNIT,AFMLINECONFIRMATIONID,DATAAREAID,TRANSDATE,AFMVENDACCOUNT,AFMISAVAILABLE,AFRISSCHEDULEDONLINE,DLVMODE,LOGISTICSPOSTALADDRESS,ShippingDateRequested,ReceiptDateRequested) 
SELECT @Channel,@AXCustomerAccountNum,@InventLocationID,itemid,Linenum,@IsNegativeValue*grossprice,netprice,netprice,@IsNegativeValue*tax,@IsNegativeValue*grossprice,@IsNegativeValue*1,@TAXGROUP,@TAXITEMGROUP,@TranID,Unit,AFMLineConfirmationID,@DataReadID,getdate(),AFMVendorID,@Afmisavailable,@Afrisscheduledonline,DLVMode,@logisticPostalAddressID,getdate()+10,getdate()+10 from @ServiceLineItem

--Insert Affiliation details
INSERT INTO [ax].[RETAILTRANSACTIONAFFILIATIONTRANS] (AFFILIATION,CHANNEL,TRANSACTIONID,CREATEDDATETIME,MODIFIEDDATETIME,DATAAREAID)
SELECT @DefaultAffiliation,@Channel,@TranID,getdate(),getdate(),@DataReadID

END
COMMIT 
IF( @@Trancount <> 0 )
SET @RES = 'false';
SELECT @RES
END
END TRY
BEGIN CATCH
          --Setting the error details into variables                         
          SELECT @Error_Number = Error_number(), 
                 @Error_Severity = Error_severity(), 
                 @Error_Module = 'Agr.OrderImport', 
                 @Error_Message = Error_message(), 
                 @Error_Line = Error_line(), 
                 @Error_State = Error_state() 
--RAISERROR (@Error_Message,@Error_Severity,@Error_State) 
IF @@TRANCOUNT>0
ROLLBACK
SELECT @RES
END CATCH

-- BELOW ARE HARDCODED AS WE DON'T HAVE LATEST XML DATA
---------------------------------------------------------
---------------------------------------------------------
--COMMENTS
-- AFMLineConfirmationID hard coded for now
-- lINENUM in paymenttrans is hard coded
-- lINENUM in RETAILTRANSACTIONSALESTRANS is hard coded
-- UNIT in RETAILTRANSACTIONSALESTRANS is hard coded
-- Quantity in a product line hard coded for now as structure needs to be changed from Lyon team 
-- CARD TYPEID is hard coded
-- Negative sign appended hard coded as those are receivables from the system
-- Transaction ID generated here temporarly it should read from lyons if they share it in XML file
---------------------------------------------------------
---------------------------------------------------------

