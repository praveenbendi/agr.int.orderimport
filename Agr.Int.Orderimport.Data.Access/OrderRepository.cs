﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Agr.Int.OrderImport.Common.Data;
using Agr.Int.Orderimport.Data.Access.Constants;
using System.Data;
using Agr.Int.OrderImport.Common.Abstractions.Repositories;
using Agr.Int.OrderImport.Common.Abstractions;
using Agr.Int.OrderImport.Data.Models.Mappers;
using Newtonsoft.Json;
using CRTBase;


namespace Agr.Int.Orderimport.Data.Access
{
    public class OrderRepository : AsyncDbRepository, IOrderRepository
    {
        private readonly IResourceLoader _resourceLoader;
        List<complexTypeAddress> listShippingAddress = null;
        List<complexTypeAddress> listBillingAddress = null;
        string initialKitNumber = string.Empty;
        int kitSequenceNumber = 0;

        public OrderRepository(IDbConnectionFactory connectionFactory, IResourceLoader resourceLoader) : base(connectionFactory)
        {
            if (resourceLoader == null) throw new ArgumentNullException(nameof(resourceLoader));
            _resourceLoader = resourceLoader;
        }

        public async Task<bool> WriteOrder(orders orders)
        {
            try
            {
                string logisticPostalAddressID = String.Empty;
                string AXCustomerAccountNum = String.Empty;
                var productLineItem = GetCustomizedProductLineItems(orders);
                var listServiceLineitem = GetCustomizedServiceItems(orders, productLineItem.Count);
                var payments = GetCustomizedPaymentDetails(orders);
                var OnlineDeliveryDateModule = orders.order[0].shipments != null && orders.order[0].shipments.FirstOrDefault() != null && orders.order[0].shipments.FirstOrDefault().customattributes != null ? orders.order[0].shipments.FirstOrDefault().customattributes.Where(j => j.attributeid == XMLAttributeType.RequestedDeliveryDatetime).FirstOrDefault() : null;
                var OnlineDeliverDate = OnlineDeliveryDateModule != null ? OnlineDeliveryDateModule.Text[0] : null;
                listShippingAddress = new List<complexTypeAddress>() { orders.order[0].shipments[0].shippingaddress }; // Supports multiple shipping 
                listShippingAddress.ForEach(t => {
                    if (t.postalcode != null)
                        t.postalcode = t.postalcode.Substring(0, 5);
                });
                listBillingAddress = new List<complexTypeAddress>() { orders.order[0].customer.billingaddress }; // supports multiple addresses for multiple payments
                listBillingAddress.ForEach(t => {
                    if (t.postalcode != null)
                        t.postalcode = t.postalcode.Substring(0, 5);
                });
                CustomerHandler custHandler = new CustomerHandler();
                Tuple<string, string> CustomerIDWithPoatalAddress = custHandler.GetCustomerWithPostalAddressID(orders.order[0].customer, listShippingAddress, listBillingAddress);
                logisticPostalAddressID = CustomerIDWithPoatalAddress.Item2;
                AXCustomerAccountNum = CustomerIDWithPoatalAddress.Item1;
                productLineItem.AsTableValuedParameter(TableTypes.ProductLineTvp, new[] { "productid", "netprice", "tax", "grossprice", "AFMLineConfirmationID", "DLVMode", "Quantity", "Unit", "Linenum", "FulfillerName", "BestDate", "BeginDateRange", "EndDateRange", "AFMMessage", "AFMVendorID", "AFMKitItemID", "AFMKitSequenceNumber", "AFMKitItemQuantity" });
                listServiceLineitem.AsTableValuedParameter(TableTypes.ServiceLineItmesTvp, new[] { "itemid", "netprice", "tax", "grossprice", "AFMLineConfirmationID", "DLVMode", "Quantity", "Unit", "Linenum" });
                payments.AsTableValuedParameter(TableTypes.PaymentTvp, new[] { "amount", "CardType", "CardNumber", "currency", "authorizationId", "lineNum", "PaymentAuthorization", "lineConfirmationId" });                
                var sql = _resourceLoader.GetContentResource(Query.WriteOrderSQL);
                var result = await WithConnection(async c => await c.QueryAsync(sql: sql,
                    param: new
                    {
                        logisticPostalAddressID,
                        AXCustomerAccountNum,
                        orders.order[0].orderno,
                        orders.order[0].customer.customeremail,
                        orders.order[0].customer.customerno,
                        orders.order[0].customer.customername,
                        orders.order[0].currency,
                        orders.order[0].orderdate,
                        Payments = payments.AsTableValuedParameter(TableTypes.PaymentTvp, new[] { "amount", "CardType", "CardNumber", "currency", "authorizationId", "lineNum", "PaymentAuthorization", "lineConfirmationId" }),
                        ProductLineItems = productLineItem.AsTableValuedParameter(TableTypes.ProductLineTvp, new[] { "productid", "netprice", "tax", "grossprice", "AFMLineConfirmationID", "DLVMode", "Quantity", "Unit", "Linenum", "FulfillerName", "BestDate", "BeginDateRange", "EndDateRange", "AFMMessage", "AFMVendorID", "AFMKitItemID", "AFMKitSequenceNumber", "AFMKitItemQuantity" }),
                        ServiceLineItem = listServiceLineitem.AsTableValuedParameter(TableTypes.ServiceLineItmesTvp, new[] { "itemid", "netprice", "tax", "grossprice", "AFMLineConfirmationID", "DLVMode", "Quantity", "Unit", "Linenum", "AFMVendorID" }),
                        ShippingAddress = listShippingAddress.AsTableValuedParameter(TableTypes.ShippingAddressTvp, new[] { "firstname", "lastname", "address1", "city", "postalcode", "statecode", "countrycode", "phone" }),
                        BillingAddress = listBillingAddress.AsTableValuedParameter(TableTypes.BillingAddressTvp, new[] { "firstname", "lastname", "address1", "city", "postalcode", "statecode", "countrycode", "phone" }),
                        Res = "true"
                        //OnlineDeliverDate = OnlineDeliverDate
                    }, // result value on success of insert
                    commandType: CommandType.Text));
                return true; /// [TODO] this acknowledgement will be removed with post implementation of Listner module from different console Application.
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        private List<complexTypeProductLineItem> GetCustomizedProductLineItems(orders orders)
        {
            try
            {
                var test = orders.order[0].productlineitems[0].customattributes.Where(j => j.attributeid == XMLAttributeType.PartOfKitNumber).FirstOrDefault();
                int lineNum = 1;
                kitSequenceNumber = 1;
                var KitAttribute = orders.order[0].productlineitems.FirstOrDefault().customattributes.Where(j => j.attributeid == XMLAttributeType.PartOfKitNumber).FirstOrDefault();
                initialKitNumber = KitAttribute != null ? KitAttribute.Text[0] : string.Empty;
                return orders.order[0].productlineitems.Select(k => new complexTypeProductLineItem
                {
                    /// [TODO] Optimization needed for below ATP calls 
                    AFMLineConfirmationID = k.customattributes != null  && k.customattributes.Where(j => j.attributeid == XMLAttributeType.AFMLineConfirmationID).FirstOrDefault() != null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.AFMLineConfirmationID).FirstOrDefault().Text[0] : String.Empty,
                    DLVMode = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.DeliveryMethod).FirstOrDefault()!= null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.DeliveryMethod).FirstOrDefault().Text[0] : String.Empty,
                    FulfillerName = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.FulfillerName).FirstOrDefault()!= null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.FulfillerName).FirstOrDefault().Text[0] : String.Empty,
                    productid = k.productid,
                    netprice = k.netprice,
                    tax = k.tax,
                    grossprice = k.grossprice,
                    Quantity = k.quantity != null ? k.quantity.Value : 0,
                    Unit = k.quantity != null ? (k.quantity.unit == String.Empty ? XMLAttributeType.DefaultUnit : k.quantity.unit) : string.Empty,
                    BestDate = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.AtpResponseBestDate).FirstOrDefault() != null ? Convert.ToDateTime(k.customattributes.Where(j => j.attributeid == XMLAttributeType.AtpResponseBestDate).FirstOrDefault().Text[0]) : XMLAttributeType.DefaultATPDate,
                    BeginDateRange = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.AtpResponseBeginDateRange).FirstOrDefault() != null ? Convert.ToDateTime(k.customattributes.Where(j => j.attributeid == XMLAttributeType.AtpResponseBeginDateRange).FirstOrDefault().Text[0]) : XMLAttributeType.DefaultATPDate,
                    EndDateRange = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.AtpResponseEndDateRange).FirstOrDefault() != null ? Convert.ToDateTime(k.customattributes.Where(j => j.attributeid == XMLAttributeType.AtpResponseEndDateRange).FirstOrDefault().Text[0]) : XMLAttributeType.DefaultATPDate,
                    IsAvailable = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.IsAvailable).FirstOrDefault() != null ? Convert.ToBoolean(k.customattributes.Where(j => j.attributeid == XMLAttributeType.IsAvailable).FirstOrDefault().Text[0]) : true, /// Default value set as true since its a part of order
                    //AFMVendorID = k.customattributes != null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.).FirstOrDefault().Text[0] : String.Empty,
                    AFMVendorID = "4444400_100",
                    AFMMessage = k.customattributes != null && k.customattributes.Where(j => j.attributeid == XMLAttributeType.InventoryAvailabilityMessage).FirstOrDefault() != null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.InventoryAvailabilityMessage).FirstOrDefault().Text[0] : string.Empty,
                    AFMKitItemID = k.customattributes != null ? (k.customattributes.Where(j => j.attributeid == XMLAttributeType.PartOfKitNumber).FirstOrDefault()!= null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.PartOfKitNumber).FirstOrDefault().Text[0] :string.Empty) : string.Empty,
                    AFMKitSequenceNumber = k.customattributes != null ? GenerateKitSequenceNumber(k.customattributes.Where(j => j.attributeid == XMLAttributeType.PartOfKitNumber) != null ? k.customattributes.Where(j => j.attributeid == XMLAttributeType.PartOfKitNumber).FirstOrDefault() : null, lineNum) : 0,
                    //AFMKitItemQuantity = k.quantity.Value, // This needs to be changed according to future XML file. Currently Lyons won't send this value
                    Linenum = lineNum++       // We can't use position as a line number from XML
                }).ToList();
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        private int GenerateKitSequenceNumber(sharedTypeCustomAttribute customAttribute,int lineNum)
        {
            if (customAttribute == null || (customAttribute != null && customAttribute.Text[0] == string.Empty))
                return 0;
                if (customAttribute != null && customAttribute.Text[0] != string.Empty)
                if (lineNum == 1)
                    kitSequenceNumber =  1;
                else if (customAttribute.Text[0] != string.Empty && customAttribute.Text[0] != initialKitNumber)
                {
                    kitSequenceNumber++;
                    initialKitNumber = customAttribute.Text[0];
                }
            return kitSequenceNumber;
        }

        private List<complexTypeShippingLineItem> GetCustomizedServiceItems(orders orders, int lineNum)
        {
            try
            {
                List<complexTypeShippingLineItem> listServiceItems = new List<complexTypeShippingLineItem>();
                var shippingLineItems = orders.order[0].shippinglineitems.FirstOrDefault().customattributes; /// Have to consider first object since we are getting only one shipping line item
                var shippingItems = JsonConvert.DeserializeObject<ServiceLevelDeliveryModes>(shippingLineItems.Where(j => j.attributeid == XMLAttributeType.ServiceItemPriceDetails).FirstOrDefault().Text[0]);
                /// Add HD service item Parameters
                if(shippingItems!=null && shippingItems.HD!=null)
                listServiceItems.Add(new complexTypeShippingLineItem
                {
                    AFMLineConfirmationID = shippingItems.HD.lineConfirmationId,
                    DLVMode = XMLAttributeType.HDServiceItemDLVMode,
                    itemid = XMLAttributeType.HDServiceItemID,
                    netprice = shippingItems.HD.netPrice,
                    tax = shippingItems.HD.tax,
                    grossprice = shippingItems.HD.grossPrice,
                    Quantity = XMLAttributeType.DefaultServiceItemQty,
                    Unit = XMLAttributeType.DefaultUnit,
                    Linenum = ++lineNum,
                    AFMVendorID = "4444400_100"
                });
                /// Add DS service item Parameters
                if (shippingItems != null && shippingItems.DS != null)
                {
                    if (shippingItems.DS.grossPrice > 0)
                    {
                        listServiceItems.Add(new complexTypeShippingLineItem
                        {
                            AFMLineConfirmationID = shippingItems.DS.lineConfirmationId,
                            DLVMode = XMLAttributeType.DSServiceItemDLVMode,
                            itemid = XMLAttributeType.DSServiceItemID,
                            netprice = shippingItems.DS.netPrice,
                            tax = shippingItems.DS.tax,
                            grossprice = shippingItems.DS.grossPrice,
                            Quantity = XMLAttributeType.DefaultServiceItemQty,
                            Unit = XMLAttributeType.DefaultUnit,
                            Linenum = ++lineNum,
                            AFMVendorID = "8888300_480"
                        });
                    }
                }

                return listServiceItems;
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        private List<TenderLine> GetCustomizedPaymentDetails(orders orders)
        {
            try
            {
                int lineNum = 1;
                var payments = orders.order[0].payments;
                List<PaymentTransaction> listPaymenttransaction = new List<PaymentTransaction>();
                List<TenderLine> listTenderLine = new List<TenderLine>();
                /// Adding credit card Payments
                listPaymenttransaction = payments.Select(k => new PaymentTransaction
                {
                    Amount = k.amount,
                    CardNumber = ((complexTypeCreditCard)k.Item).cardnumber,
                    CardType = ((complexTypeCreditCard)k.Item).cardtype,
                    LineNum = lineNum++,
                    PaymentType = typeof(complexTypeCreditCard),
                    TenderData = JsonConvert.DeserializeObject<List<TenderLine>>(k.customattributes.Where(j => j.attributeid == XMLAttributeType.AuthorizeData).FirstOrDefault().Text[0])
                }).Where(k => k.PaymentType == typeof(complexTypeCreditCard)).ToList();
                /// Adding custom Payments
                listPaymenttransaction.Union(payments.Select(k => new PaymentTransaction
                {
                    Amount = k.amount,
                    CardNumber = ((complexTypeCreditCard)k.Item).cardnumber,
                    CardType = ((complexTypeCreditCard)k.Item).cardtype,
                    LineNum = lineNum++,
                    PaymentType = typeof(complexTypeCustomPaymentMethod),
                    TenderData = JsonConvert.DeserializeObject<List<TenderLine>>(k.customattributes.Where(j => j.attributeid == XMLAttributeType.AuthorizeData).FirstOrDefault().Text[0])
                }).Where(k => k.PaymentType == typeof(complexTypeCustomPaymentMethod)).ToList());
                lineNum = 1;
                /// Customize at each auth level
                foreach (PaymentTransaction paymentTransaction in listPaymenttransaction)
                    foreach (TenderLine tenderline in paymentTransaction.TenderData)
                    {
                        tenderline.CardNumber = paymentTransaction.CardNumber;
                        tenderline.CardType = paymentTransaction.CardType;
                        tenderline.lineNum = lineNum++;
                        tenderline.PaymentAuthorization = !string.IsNullOrEmpty(tenderline.PaymentAuthorization) ? string.Format(tenderline.PaymentAuthorization, tenderline.authorizationId, tenderline.lineConfirmationId) : string.Empty;
                        listTenderLine.Add(tenderline);
                    }
                return listTenderLine;
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.StackTrace);
                Console.ReadKey();
                throw; }
        }


        /// <summary>
        /// This function is no longer used as per the new XML structure
        /// </summary>
        /// <param name="orders"></param>
        /// <param name="sku"></param>
        /// <param name="MOD"></param>
        /// <returns></returns>
        private Entity GetCustomizedATPDetails(orders orders,String sku,string MOD)
        {
            try
            {
                string AFMDSVendorID = string.Empty; /// This is not acutal ID. It is combination of  Account number and shipTo -> account_shipto for DS
                string AFMHDVendorID = string.Empty; /// This is not acutal ID. It is combination of  Account number and shipTo -> account_shipto for HD
                Entity entity = new Entity();
                /// the term 'actual' used because this is same response we are passing. which is different from ATP based on MOD 
                var actualDSATP = JsonConvert.DeserializeObject<ATP>(orders.order[0].customattributes.Where(j => j.attributeid == XMLAttributeType.DirectShipATPResponse).FirstOrDefault().Text[0]);
                var actualHDATP = JsonConvert.DeserializeObject<ATP>(orders.order[0].customattributes.Where(j => j.attributeid == XMLAttributeType.HomeDeliveryATPResponse).FirstOrDefault().Text[0]);
                AFMDSVendorID = String.Format("{0}_{1}", actualDSATP.properties.accountNumber, actualDSATP.properties.shipTo);
                AFMHDVendorID = String.Format("{0}_{1}", actualHDATP.properties.accountNumber, actualHDATP.properties.shipTo);
                if (MOD == DeliveryMode.HDDLVMode)
                {
                    entity = actualHDATP.entities.Where(k=>k.sku == sku).FirstOrDefault();
                    entity.AFMVendorID = AFMHDVendorID;
                }
                if (MOD == DeliveryMode.DSDLVMode || MOD == DeliveryMode.DSThirdparty)
                {
                    entity = actualDSATP.entities.Where(k => k.sku == sku).FirstOrDefault();
                    entity.AFMVendorID = AFMDSVendorID;
                }
                return entity;
            }
            catch(Exception exe)
            {
                Console.WriteLine(exe.StackTrace);
                Console.ReadKey();
                throw;
            }
        }

        private complexTypeAddress GetCustomizedAddress(orders orders)
        {

            try
            {
                return orders.order[0].shipments[0].shippingaddress;
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.StackTrace);
                Console.ReadKey();
                throw;
            }
        }
    }

}