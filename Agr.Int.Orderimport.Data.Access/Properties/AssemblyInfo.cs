﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Agr.Int.Orderimport.Data.Access")]
[assembly: AssemblyDescription("SFCC Import Order Data Access")]
[assembly: AssemblyCompany("Ashley Furniture Industries, Inc.")]
[assembly: AssemblyProduct("Ashley Global Retail Order import")]
[assembly: AssemblyCopyright("Copyright © 2017 Ashley Furniture Industries, Inc.")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("35562a86-97f1-441c-9e43-6045e869e70b")]

#if DEBUG
[assembly: AssemblyConfiguration("DEBUG")]
#else
[assembly: AssemblyConfiguration("RELEASE")]
#endif

/*	Error: fatal: No names found, cannot describe anything.


	Branch: master
	Major: 1
	Minor: 0
	Patch: 0
	Build Number: 0
	Initial Patch: 0
	Commit Count: 0
	Is Dirty?: False
	Version: 1.0.0.0
	Is Release Branch?: False
	Product Version: 1.0.0
	Host Path: C:\BitbucketRepository\Order Import\Agr.Int.OrderImport\Agr.Int.Orderimport.Data.Access\Properties
	Output: 
master

*/

// Version information for an assembly consists of the following four values: Major Version, Minor Version, Build Number, Revision

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]